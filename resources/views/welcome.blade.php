<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sanctuary</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
</head>

<body>
    <div class="relative min-h-screen bg-gray-800">
        <div
            class="absolute text-center text-white transform -translate-x-1/2 -translate-y-1/2 font-lato top-1/2 left-1/2">
            <h1 class="mb-3 text-3xl">Sanctuary Back</h1>
            <p>Use <code class="px-1 mx-1 bg-gray-700 rounded-sm">laravel/sanctum</code> to offer auth for <code
                    class="px-1 mx-1 bg-gray-700 rounded-sm">flutter</code>
                application with token.</p>
        </div>
    </div>
</body>

</html>
