<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BookController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\ArticleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthController::class, 'token'])->name('api.auth.login');
Route::post('/sanctum/token', [AuthController::class, 'token'])->name('api.auth.token.get');

Route::get('/articles', [ArticleController::class, 'index'])->name('api.articles.index');

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/user', [UserController::class, 'current'])->name('api.user');

    Route::get('/sanctum/revoke', [AuthController::class, 'revoke'])->name('api.auth.revoke');

    Route::get('/books', [BookController::class, 'index'])->name('api.books.index');
});
