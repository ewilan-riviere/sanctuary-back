<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \App\Models\User $resource
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'   => $this->resource->name,
            'email'  => $this->resource->email,
            'avatar' => 'https://eu.ui-avatars.com/api/?name='.$this->resource->name.'&color=7F9CF5&background=EBF4FF',
        ];
    }
}
