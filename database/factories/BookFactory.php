<?php

namespace Database\Factories;

use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'       => ucfirst($this->faker->words(3, true)),
            'author'      => $this->faker->name,
            'page_count'  => $this->faker->numberBetween('200', '1200'),
            'isbn'        => $this->faker->isbn13,
            'summary'     => $this->faker->text,
        ];
    }
}
