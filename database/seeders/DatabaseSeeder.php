<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\Article;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        // \App\Models\User::factory(10)->create();
        Article::factory(10)->create();
        Book::factory(10)->create();
    }
}
